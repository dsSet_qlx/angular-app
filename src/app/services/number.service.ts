import { Injectable } from '@angular/core';

@Injectable()
export class NumberService {

  constructor() { }

  public generateNumber(min: number = 0, max: number = 100): number {
    const delta = max - min;
    const randomNumber = Math.round(Math.random() * delta);
    return min + randomNumber;
  }

}
