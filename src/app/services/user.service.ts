import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserModel } from '../models/UserModel';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient
  ) { }

  public createUserForm = (user: Partial<UserModel> = {}): FormGroup => {
    const form = new FormGroup({
      _id: new FormControl(user._id),
      firstName: new FormControl(user.firstName, [Validators.required]),
      lastName: new FormControl(user.lastName),
      email: new FormControl(user.email),
      bio: new FormControl(user.bio),
      phone: new FormControl(user.phone),
      isVerified: new FormControl(user.isVerified),
      birthday: new FormControl(user.birthday),
    });

    return form;
  }

  public getUserList = (): Observable<Array<Partial<UserModel>>> => {
    return this.http.get<Array<Partial<UserModel>>>('http://localhost:3001/user');
  }

  public getUser = (id: string): Observable<UserModel> => {
    return this.http.get<UserModel>(`http://localhost:3001/user/${id}`);
  }

  public createUser(user: UserModel): Observable<void> {
    return this.http.post<void>(`http://localhost:3001/user`, user);
  }

  public updateUser(user: UserModel): Observable<void> {
    return this.http.put<void>(`http://localhost:3001/user/${user._id}`, user);
  }

}
