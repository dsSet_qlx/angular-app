

export interface UserModel {

  _id: string;

  firstName: string;

  lastName: string;

  email: string;

  bio: string;

  phone: string;

  isVerified: boolean;

  birthday: string;

}
