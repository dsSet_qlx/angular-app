import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersPageComponent } from './components/users-page/users-page.component';
import { UserDetailsPageComponent } from './components/user-details-page/user-details-page.component';

// url: /user/123

const routes: Routes = [
  {
    path: 'users',
    component: UsersPageComponent
  },
  {
    path: 'user/:id',
    component: UserDetailsPageComponent
  },
  {
    path: 'user',
    component: UserDetailsPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
