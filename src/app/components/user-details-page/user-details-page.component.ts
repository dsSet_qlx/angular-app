import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { UserService } from '../../services/user.service';
import { UserModel } from '../../models/UserModel';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-user-details-page',
  templateUrl: './user-details-page.component.html',
  styleUrls: ['./user-details-page.component.scss']
})
export class UserDetailsPageComponent implements OnInit {

  public userForm?: FormGroup;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
  ) { }

  ngOnInit(): void {
    this.userForm = this.userService.createUserForm();

    this.route.params.pipe(
      map(this.getIdFromParams),
      mergeMap(this.createUserModel),
    ).subscribe(this.handleParams);
  }

  public handleSave = () => {
    if (this.userForm?.valid) {
      const model: UserModel = this.userForm?.value;
      const source = model._id
        ? this.userService.updateUser(model)
        : this.userService.createUser(model);

      source.pipe(
        catchError(this.handleError),
      ).subscribe(this.navigateToUserList);
    }
  }

  public navigateToUserList = () => {
    this.router.navigateByUrl('/users');
  }

  private createUserModel = (id: string | null): Observable<Partial<UserModel>>  => {
    return id ? this.userService.getUser(id) : of({ phone: '5555-' });
  }

  private handleParams = (user: Partial<UserModel>) => {
    this.userForm?.reset(user);
  }

  private getIdFromParams = (params: Params): string | null => {
    return params?.id;
  }

  private handleError = (error: HttpErrorResponse) => {
    window.alert(error.message);
    return of(null);
  }

}
