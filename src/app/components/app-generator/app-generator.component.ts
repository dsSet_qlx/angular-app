import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NumberService } from '../../services/number.service';

@Component({
  selector: 'app-generator',
  templateUrl: './app-generator.component.html',
  styleUrls: ['./app-generator.component.scss']
})
export class AppGeneratorComponent implements OnInit {

  @Input() public min?: number;

  @Input() public max?: number;

  @Output() public generate = new EventEmitter<number>();

  public randomNumber = 0;

  public get isInvalidProps(): boolean {
    return (this.max && this.min) ? this.max <= this.min : false;
  }

  constructor(
    private numberService: NumberService
  ) { }

  ngOnInit(): void {
    this.handleGenerateButtonClick();
  }

  public handleGenerateButtonClick(): void {
    this.randomNumber = this.numberService.generateNumber(this.min, this.max);
    this.generate.emit(this.randomNumber);
  }

}
