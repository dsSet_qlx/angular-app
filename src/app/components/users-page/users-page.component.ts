import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { UserModel } from '../../models/UserModel';
import { map, tap } from 'rxjs/operators';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-users-page',
  templateUrl: './users-page.component.html',
  styleUrls: ['./users-page.component.scss']
})
export class UsersPageComponent implements OnInit {

  public users: Array<Partial<UserModel>> = [];

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.userService.getUserList().subscribe(this.handleUserListLoad);
  }

  public getUserUrl(user: Partial<UserModel>): string {
    return `/user/${user._id}`;
  }

  private handleUserListLoad = (list: Array<Partial<UserModel>>) => {
    this.users = list;
  }

}
