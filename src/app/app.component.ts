import { Component } from '@angular/core';

interface GeneratorContext {
  min: number;
  max: number;
  id: number;
}

@Component({
  selector: 'app-root',
  template: `
    <h1>User Application</h1>
    <router-outlet></router-outlet>
  `,
  // templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  styles: [
    `
      .h1 {
        text-decoration: underline;
      }
    `
  ],
})
export class AppComponent {
  public title = 'angular-app';

  public generators: Array<GeneratorContext> = [
    { min: 1, max: 10, id: 1 },
    { min: 100, max: 10, id: 2 },
    { min: 999, max: 1025, id: 3 },
  ];

  private invisibleVariable = 'hidden';

  public handleGenerate(value: number): void {
    console.log('Generate', value);
  }

}
